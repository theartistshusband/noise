use nannou::noise::{BasicMulti, Seedable};
use nannou::noise::NoiseFn;
use nannou::prelude::*;
use rand::random;

fn main() {
    nannou::app(model)
        .update(update)
        .run();
}

#[derive(Debug)]
struct Particle {
    position: Point2,
}

impl Particle {
    fn new(x: f32, y: f32) -> Self {
        Particle {
            position: pt2(x, y),
        }
    }

    fn update(&mut self, direction: f64) {
        let dx = direction.cos();
        let dy = direction.sin();
        self.position += pt2(dx as f32, dy as f32);
    }
}

struct Model {
    noise: BasicMulti,
    particles: Vec<Particle>,
}

fn model(app: &App) -> Model {
    app
        .new_window()
        .size(600, 600)
        .view(view)
        .build()
        .unwrap();
    let rect = app.window_rect();

    let noise = BasicMulti::new().set_seed(random());
    let mut particles = Vec::new();
    for _i in 0..3000 {
        let x = random_f32() * rect.w() + rect.left();
        let y = random_f32() * rect.h() + rect.bottom();
        particles.push(Particle::new(x, y))
    }

    Model { noise, particles }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let t = app.elapsed_frames() as f64 / 50000.;
    for i in 0..model.particles.len() {
        let particle = &mut model.particles[i];
        let direction = 15. * model.noise.get([particle.position[0] as f64 / 400., particle.position[1] as f64 / 400., t]);
        particle.update(direction);
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    let t = (app.elapsed_frames() as f32) / 10000.;
    for particle in &model.particles {
        draw.ellipse().xy(particle.position).w_h(1., 1.).hsla(0.1 + t.sin(), 1. + t / 10000., 5., 0.01);
    }

    draw.to_frame(app, &frame).unwrap();
}